***Пример простого деплоя с копированием всех файлов:***
```yaml
variables:
  PROJECT_DIR_TEST: "/var/www/developer/data/www/for-ip.design-lab.ru"
  PROJECT_DIR_PROD: "/var/www/developer/data/www/for-ip.design-lab.ru"

before_script:
    - "echo \"START BUILD ${CI_COMMIT_REF_NAME}\""

after_script:
    - "echo \"END BUILD ${CI_COMMIT_REF_NAME}\""

deploy_test:
    tags:
        - test
    only:
        - test
    script:
        - "sudo rsync -av --exclude='*.git*' ./ ${PROJECT_DIR_TEST}"
        - "sudo chown -R developer:developer ${PROJECT_DIR_TEST}"
        
deploy_prod:
    tags:
        - prod
    only:
        - master
    script:
        - "sudo rsync -av --exclude='*.git*' ./ ${PROJECT_DIR_PROD}"
        - "sudo chown -R developer:developer ${PROJECT_DIR_PROD}"
```
***Примечания***:
> Очень желательно устанавливать переменные типа PROJECT_DIR в настройках проекта, а не в yml-файле. 

> Если в проекте удаляются файлы, перед rsync надо сделать чистку, т.е. rm -f {FILES}

> Следует обратить внимание на пользователя в chown, для каждого проекта или ряда проектов он свой. По умолчанию раннер выполняет сборку от root (если тип - ssh), соответственно неообходимо в конце всех операций установить истинного владельца через команду chown. Если раннер работает от конкретного пользователя - chown не нужен.

> В переменной tags указан тег для раннера, он должен совпадать с тегом/тегами указанным в раннере. Например в проекте подключены 2 раннера с тегами test & prod, таким образом можно указывать 1 и более конкретных раннеров, которые запускают этот билд

> В переменной only обычно указывается ветка, которую раннер должен собирать. Если к примеру, надо собирать все кроме master - то вместо only необходимо использовать except (или вместе)
