<?php

use Phpmig\Migration\Migration;

class FirstMigration extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = file_get_contents("dump.sql");
        $container = $this->getContainer();
        $container['db']->query($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        // clear all
    }
}
