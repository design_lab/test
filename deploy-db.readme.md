## Деплой базы данных

### 1. Деплой новой базы
Все достаточно просто, заливаем через команду mysql файл с дампом бд:
```bash
mysql --user="${DB_USER}" --database="${DB_NAME}" --password="${DB_PASSWORD}" < "${DUMP_PATH}"
```
Переменные можно делать для деплоя на каждое окружения свои (указываем в gitlab проекте), например можно выставить 2 разных переменных **DB_NAME_PROD** & **DB_NAME_DEV**, и использовать их в разных тегах gitlab-ci.yml, к примеру так:
```yaml
deploy_dev:
    tags:
        - dev
    only:
        - dev
    script:
        - "rsync -av --exclude='*.git*' ./ ${PROJECT_DIR_DEV}"
        - "mysql --user=\"${DB_USER_DEV}\" --database=\"${DB_NAME_DEV}\" --password=\"${DB_PASSWORD_DEV}\" < \"${DUMP_PATH_DEV}\"}"

deploy_prod:
    tags:
        - prod
    only:
        - master
    script:
        - "rsync -av --exclude='*.git*' ./ ${PROJECT_DIR_PROD}"
        - "mysql --user=\"${DB_USER_PROD}\" --database=\"${DB_NAME_PROD}\" --password=\"${DB_PASSWORD_PROD}\" < \"${DUMP_PATH_PROD}\"}"
```
**Для обновления существующей бд необходимо использовать миграции. Если платформа их не предусматривает (в данном случае Netcat - нет) - делать на основе любого инструмента, к примеру [https://packagist.org/packages/davedevelopment/phpmig](https://packagist.org/packages/davedevelopment/phpmig)
В идеале - лучше делать с нуля миграции (т.е. первой миграцией по сути является заливка дампа), чем заливать именно дамп через командную строку (это можно сделать только первый раз, в дальнейшем он будет затирать изменения)**

### 2. Деплой конфигов
В случае использования платформ, которые не поддерживают системные env, деплоить можно копированием файлов (заранее подготовленных в репозитории) или заменой переменных при непосредственном разворачивании.
Представим что есть конфиг vars.inc.php с определенными переменными:
```php
$MYSQL_HOST = "127.0.0.1";
$MYSQL_USER = "user";
$MYSQL_PASSWORD = "pass";
$MYSQL_DB_NAME = "dbname";
$MYSQL_CHARSET = "utf8";
$MYSQL_ENCRYPT = "MD5";
```
Самый простой вариант - сделать подстановку по шаблону, для этого нужны тексты для замены:
```php
$MYSQL_HOST = "{MYSQL_HOST}";
$MYSQL_USER = "{MYSQL_USER}";
$MYSQL_PASSWORD = "{MYSQL_PASSWORD}";
$MYSQL_DB_NAME = "{MYSQL_DBNAME}";
$MYSQL_CHARSET = "utf8";
$MYSQL_ENCRYPT = "MD5";
```
И делаем перезапись в gitlab-ci.yml с заменой переменных:
```yaml
...
    script:
        - "sed -i 's/{MYSQL_HOST}/${MYSQL_HOST_DEV}/g' vars.inc.php"
        - "sed -i 's/{MYSQL_USER}/${MYSQL_USER_DEV}/g' vars.inc.php"
        - "sed -i 's/{MYSQL_PASSWORD}/${MYSQL_PASSWORD_DEV}/g' vars.inc.php"
        - "sed -i 's/{MYSQL_DBNAME}/${MYSQL_DBNAME_DEV}/g' vars.inc.php"
```

### 3. Кеш контента
При заливке сайта надо всегда учитывать наличие динамического контента (заливаемые через сайт файлы/картинки, файлы логов и сессий и т.п.). Нужно просто добавлять эти директории в exclude при выполнении rsync, например:
```yaml
...
    script:
        - "rsync -av --exclude='*.git*' --exclude='netcat_files'  --exclude='netcat_cache'  ./ ${PROJECT_DIR_DEV}"
```
### 4. Откат
Для того чтобы откатить изменения при деплое - есть обычно 2 варианта:
1. Делать откатывающий коммит (переход к нужному коммиту и выполнение revert)
2. Перезапустить pipeline с нужным коммитом на странице [https://gitlab.com/{user}/{project}/-/jobs](https://gitlab.com/{user}/{project}/-/jobs). Этот вариант можно использовать только как срочное или временное решение, потому-что после следующего коммита деплой будет перезаписан.